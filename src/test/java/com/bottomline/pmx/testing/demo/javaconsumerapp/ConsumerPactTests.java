package com.bottomline.pmx.testing.demo.javaconsumerapp;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.RequestResponsePact;
import au.com.dius.pact.core.model.annotations.Pact;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import static io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(PactConsumerTestExt.class)
public class ConsumerPactTests {

    String expectedDateString = "04/24/2021";
    String parsedExpectedDateString = new SimpleDateFormat("MM/dd/yyyy").parse(expectedDateString).toString();

    public ConsumerPactTests() throws ParseException {}

    @Pact(consumer="java-consumer", provider="dotnet-provider")
    RequestResponsePact invalidDateParameter(PactDslWithProvider builder){
        return builder.given("There is data")
                .uponReceiving("A invalid GET request for Date Validation with invalid date parameter")
                .method("GET")
                .path("/api/provider")
                .query("validDateTime=lolz")
                .willRespondWith()
                .status(400)
                .headers(Map.of("Content-Type", "application/json; charset=utf-8"))
                .body(newJsonBody(object -> {
                    object.stringType("message","validDateTime is not a date or time");
                }).build())
                .toPact();
    }

    @Pact(consumer="java-consumer", provider="dotnet-provider")
    RequestResponsePact emptyDateParameter(PactDslWithProvider builder){
        return builder.given("There is data")
                .uponReceiving("A invalid GET request for Date Validation with empty string date parameter")
                .method("GET")
                .path("/api/provider")
                .query("validDateTime=")
                .willRespondWith()
                .status(400)
                .headers(Map.of("Content-Type", "application/json; charset=utf-8"))
                .body(newJsonBody(object -> {
                    object.stringType("message","validDateTime is required");
                }).build())
                .toPact();
    }

    @Pact(consumer="java-consumer", provider="dotnet-provider")
    RequestResponsePact noData(PactDslWithProvider builder){
        return builder.given("There is no data")
                .uponReceiving("A valid GET request for Date Validation")
                .method("GET")
                .path("/api/provider")
                .query("validDateTime=04/24/2021")
                .willRespondWith()
                .status(404)
                .toPact();
    }

    @Pact(consumer="java-consumer", provider="dotnet-provider")
    RequestResponsePact parseCorrectDateParameter(PactDslWithProvider builder) throws ParseException {


        return builder.given("There is data")
                .uponReceiving("A valid GET request for Date Validation")
                .method("GET")
                .path("/api/provider")
                .query("validDateTime=" + expectedDateString )
                .willRespondWith()
                .status(200)
                .headers(Map.of("Content-Type", "application/json; charset=utf-8"))
                .body(newJsonBody(object -> {
                    object.stringType("test","NO");
                    object.stringType("validDateTime", parsedExpectedDateString);
                }).build())
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "invalidDateParameter")
    void itHandlesInvalidDateParameter(MockServer mockServer){
        RestTemplate restTemplate = new RestTemplateBuilder()
                .rootUri(mockServer.getUrl())
                .build();
        HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
                () -> new ConsumerApiClientService(restTemplate).validateDateTimeUsingProviderApi("lolz"));
        assertEquals(400, e.getRawStatusCode());
    }

    @Test
    @PactTestFor(pactMethod = "emptyDateParameter")
    void itHandlesEmptyDateParameter(MockServer mockServer){
        RestTemplate restTemplate = new RestTemplateBuilder()
                .rootUri(mockServer.getUrl())
                .build();
        HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
                () -> new ConsumerApiClientService(restTemplate).validateDateTimeUsingProviderApi(""));
        assertEquals(400, e.getRawStatusCode());
    }

    @Test
    @PactTestFor(pactMethod = "noData")
    void itHandlesNoData(MockServer mockServer){
        RestTemplate restTemplate = new RestTemplateBuilder()
                .rootUri(mockServer.getUrl())
                .build();
        HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
                () -> new ConsumerApiClientService(restTemplate).validateDateTimeUsingProviderApi("04/24/2021"));
        assertEquals(404, e.getRawStatusCode());
    }

    @Test
    @PactTestFor(pactMethod = "parseCorrectDateParameter")
    void itHandlesValidDateParameter(MockServer mockServer){
        RestTemplate restTemplate = new RestTemplateBuilder()
                .rootUri(mockServer.getUrl())
                .build();
        ApiResponse response = new ConsumerApiClientService(restTemplate).validateDateTimeUsingProviderApi("04/24/2021");
        assertEquals(parsedExpectedDateString, response.getValidDateTime());
    }
}

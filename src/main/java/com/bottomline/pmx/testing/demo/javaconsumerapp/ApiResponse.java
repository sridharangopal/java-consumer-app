package com.bottomline.pmx.testing.demo.javaconsumerapp;

import lombok.*;

@Data
public class ApiResponse {

    private String message;
    private String test;
    private String validDateTime;
}

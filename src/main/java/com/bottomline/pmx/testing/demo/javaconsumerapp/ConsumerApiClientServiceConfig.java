package com.bottomline.pmx.testing.demo.javaconsumerapp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConsumerApiClientServiceConfig {
    @Bean
    RestTemplate apiRestTemplate(@Value("${provider.port:9222}") int port){
        return new RestTemplateBuilder().rootUri(String.format("http://localhost:%d", port)).build();
    }
}

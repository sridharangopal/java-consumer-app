package com.bottomline.pmx.testing.demo.javaconsumerapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaConsumerAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaConsumerAppApplication.class, args);
	}

}

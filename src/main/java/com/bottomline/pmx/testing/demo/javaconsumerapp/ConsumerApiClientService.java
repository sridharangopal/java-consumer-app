package com.bottomline.pmx.testing.demo.javaconsumerapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ConsumerApiClientService {
    private final RestTemplate restTemplate;

    @Autowired
    public ConsumerApiClientService(RestTemplate restTemplate) { this.restTemplate = restTemplate; }

    public ApiResponse validateDateTimeUsingProviderApi(String dateTimeToValidate) {
        return restTemplate.exchange("/api/provider?validDateTime=" + dateTimeToValidate,
                HttpMethod.GET, 
                getRequestEntity(),
                new ParameterizedTypeReference<ApiResponse>(){}).getBody();
    }

    private HttpEntity<String> getRequestEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, "application/json");
        return new HttpEntity<>(headers);
    }

}
